package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        commands.stream().forEach(command -> command.execute());
    }

    @Override
    public void undo() {
        ListIterator<Command> list = commands.listIterator(commands.size());
        while (list.hasPrevious()) {
            list.previous().undo();
        }
    }
}
