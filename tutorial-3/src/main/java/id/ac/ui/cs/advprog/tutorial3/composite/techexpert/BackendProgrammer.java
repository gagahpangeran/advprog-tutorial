package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    //TODO Implement
    public BackendProgrammer(String name, double salary) {
        this.name = name;
        this.role = "Back End Programmer";
        this.minimumSalary = 20000.00;
        setSalary(salary);
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
