package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        //TODO Implement
        this.name = name;
        this.role = "CEO";
        this.minimumSalary = 200000.00;
        setSalary(salary);
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
