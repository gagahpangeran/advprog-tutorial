package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class MainComposite {
    public static void main(String[] args) {
        Company company = new Company();

        Employees dekDepe = new Ceo("Dek Depe", 1000000.0);
        Employees kakPewe = new BackendProgrammer("Kak Pewe", 50000);
        Employees sisDea = new FrontendProgrammer("Sis Dea", 50000);
        Employees bangBasdat = new UiUxDesigner("Bang Basdat", 100000);

        company.addEmployee(dekDepe);
        company.addEmployee(kakPewe);
        company.addEmployee(sisDea);
        company.addEmployee(bangBasdat);

        for (Employees employee : company.getAllEmployees()) {
            System.out.println("My name is " + employee.getName()
                                + " and my role is " + employee.getRole());
        }

        System.out.println("The company must pay all employees " + company.getNetSalaries());
    }
}
