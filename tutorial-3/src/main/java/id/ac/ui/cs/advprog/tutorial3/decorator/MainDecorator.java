package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Filling;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class MainDecorator {
    public static void main(String[] args) {
        Food sandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        sandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(sandwich);
        sandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(sandwich);

        System.out.println("I order " + sandwich.getDescription());
        System.out.println("The price is $" + sandwich.cost());

        Food burger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        burger = FillingDecorator.CHEESE.addFillingToBread(burger);
        burger = FillingDecorator.BEEF_MEAT.addFillingToBread(burger);
        burger = FillingDecorator.TOMATO.addFillingToBread(burger);
        burger = FillingDecorator.LETTUCE.addFillingToBread(burger);

        System.out.println("You order " + burger.getDescription());
        System.out.println("The price is $" + burger.cost());
    }
}
