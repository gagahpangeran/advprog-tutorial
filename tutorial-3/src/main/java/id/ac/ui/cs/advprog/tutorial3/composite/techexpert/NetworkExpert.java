package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.role = "Network Expert";
        this.minimumSalary = 50000.00;
        setSalary(salary);
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
