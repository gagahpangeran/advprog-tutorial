package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    //TODO Implement
    public FrontendProgrammer(String name, double salary) {
        this.name = name;
        this.role = "Front End Programmer";
        this.minimumSalary = 30000.00;
        setSalary(salary);
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
