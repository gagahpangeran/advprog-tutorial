package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        this.role = "UI/UX Designer";
        this.minimumSalary = 90000.00;
        setSalary(salary);
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
