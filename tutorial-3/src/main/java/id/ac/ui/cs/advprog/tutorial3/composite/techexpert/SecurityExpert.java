package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    //TODO Implement
    public SecurityExpert(String name, double salary) {
        this.name = name;
        this.role = "Security Expert";
        this.minimumSalary = 70000.00;
        setSalary(salary);
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
