package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    public void swim() {
        System.out.println("Every duck can swim");
    }

    public abstract void display();

    public void setFlyBehavior(FlyBehavior fly) {
        flyBehavior = fly;
    }

    public void setQuackBehavior(QuackBehavior quack) {
        quackBehavior = quack;
    }
}
