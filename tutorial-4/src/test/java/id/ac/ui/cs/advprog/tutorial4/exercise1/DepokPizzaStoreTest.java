package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private PizzaStore depokStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        depokStore = new DepokPizzaStore();
    }

    @Test
    public void makeVeggieDepokPizza() {
        pizza = depokStore.orderPizza("veggie");
        assertEquals("---- Depok Style Veggie Pizza ----\n"
                + "Sweet dough\n"
                + "Matah Sauce\n"
                + "Shredded Gorgonzola\n"
                + "Jengkol, Spinach, Eggplant, Black Olives\n", pizza.toString());
    }

    @Test
    public void makeCheeseDepokPizza() {
        pizza = depokStore.orderPizza("cheese");
        assertEquals("---- Depok Style Cheese Pizza ----\n"
                + "Sweet dough\n"
                + "Matah Sauce\n"
                + "Shredded Gorgonzola\n", pizza.toString());
    }

    @Test
    public void makeClamDepokPizza() {
        pizza = depokStore.orderPizza("clam");
        assertEquals("---- Depok Style Clam Pizza ----\n"
                + "Sweet dough\n"
                + "Matah Sauce\n"
                + "Shredded Gorgonzola\n"
                + "Giant Clams from Bikini Bottom\n", pizza.toString());
    }
}
