package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {
    private PizzaStore nyStore;
    private Pizza pizza;

    @Before
    public void setUp() {
        nyStore = new NewYorkPizzaStore();
    }

    @Test
    public void makeCheeseNewYorkPizza() {
        pizza = nyStore.orderPizza("cheese");
        assertEquals("---- New York Style Cheese Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n", pizza.toString());
    }

    @Test
    public void makeClamNewYorkPizza() {
        pizza = nyStore.orderPizza("clam");
        assertEquals("---- New York Style Clam Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n"
                + "Fresh Clams from Long Island Sound\n", pizza.toString());
    }

    @Test
    public void makeVeggieNewYorkPizza() {
        pizza = nyStore.orderPizza("veggie");
        assertEquals("---- New York Style Veggie Pizza ----\n"
                + "Thin Crust Dough\n"
                + "Marinara Sauce\n"
                + "Reggiano Cheese\n"
                + "Garlic, Onion, Mushrooms, Red Pepper\n", pizza.toString());
    }
}
