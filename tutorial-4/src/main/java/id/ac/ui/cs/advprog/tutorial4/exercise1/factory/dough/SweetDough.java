package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class SweetDough implements Dough {
    public String toString() {
        return "Sweet dough";
    }
}
