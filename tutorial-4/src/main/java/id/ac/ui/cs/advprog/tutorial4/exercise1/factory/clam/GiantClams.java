package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class GiantClams implements Clams {

    public String toString() {
        return "Giant Clams from Bikini Bottom";
    }
}
